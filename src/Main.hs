import Control.Monad.State.Lazy
import Control.Monad.Loops
import System.Process
import Data.Functor ((<$>))

import System.Clipboard (setClipboardString)

maxLen = 2000

-- State (remaining messages, accumulating message) finished
addLine :: Int -> State ([String], String) Bool
addLine n = do
    (ms', x) <- get
    case ms' of
        []   -> return True
        m:ms -> do
            let m' = x ++ "\n" ++ m
            _ <- put (ms, m')
            return $ length m' > n

addLines :: Int -> State [String] String
addLines n = do
    ms <- get
    return . snd . until (evalState $ addLine n) (execState $ addLine n) $ (ms, "")

repeatAddLines :: Int -> State [String] [String]
repeatAddLines n = whileM (fmap not . mapState nullState $ get) $ do
        (a, s') <- runState (addLines n) <$> get
        _ <- put s'
        return a
    where nullState (_, s) = (null s, s)

main = do
    m <- lines <$> getContents
    let ms = evalState (repeatAddLines maxLen) m
    forM_ [1..length ms] $ \i -> do
        putStrLn $ "Message " ++ show i ++ "/" ++ (show . length $ ms)
        setClipboardString $ ms !! (i - 1)
